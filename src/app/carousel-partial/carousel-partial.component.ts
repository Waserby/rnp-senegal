import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-carousel-partial',
  templateUrl: './carousel-partial.component.html',
  styleUrls: ['./carousel-partial.component.css',
              './carousel.css']
})
export class CarouselPartialComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
