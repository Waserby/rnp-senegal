import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OntactPartialComponent } from './ontact-partial.component';

describe('OntactPartialComponent', () => {
  let component: OntactPartialComponent;
  let fixture: ComponentFixture<OntactPartialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OntactPartialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OntactPartialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
