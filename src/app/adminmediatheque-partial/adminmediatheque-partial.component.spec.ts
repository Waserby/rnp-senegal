import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminmediathequePartialComponent } from './adminmediatheque-partial.component';

describe('AdminmediathequePartialComponent', () => {
  let component: AdminmediathequePartialComponent;
  let fixture: ComponentFixture<AdminmediathequePartialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminmediathequePartialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminmediathequePartialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
