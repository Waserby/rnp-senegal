import { Component, OnInit } from '@angular/core';
import { Theme } from '../../models/theme-model';
import { ThemeFBService } from '../theme-fb.service';
import { ThemeService } from '../theme.service';
import { AngularFireList } from '../../../node_modules/angularfire2/database';
import { Observable, Subject } from '../../../node_modules/rxjs';
import { Router } from '../../../node_modules/@angular/router';
// import { ThemeService } from '../theme.service';

@Component({
  selector: 'app-forum-page',
  templateUrl: './forum-page.component.html',
  styleUrls: ['./forum-page.component.css']
})
export class ForumPageComponent implements OnInit {
    TabThemesMock : any;//Ce tableau est use pour l'ajout des programmes dans Firebase
    TabThemesFireBaseSnap : any;
  //Pour la recherche, on crée un tableau qui contiendra toujours TOUS lesThemes de Firebase (il ne chagne jamais)
    loadedThemesList:Array<Theme>;
  

  constructor (private router : Router ,private themeFbService :ThemeFBService, private themeService : ThemeService ) { 
  }

  ngOnInit() {
    //this.ajoutAllThemesFirebase(); a decommenter pour ajouter le mock à Firebase
    this.getListThemesFirebase(); 
  }
  
  //------------------------------Pour la recherche----------------------------------
      initializeItems(): void {
        this.TabThemesFireBaseSnap = this.loadedThemesList;
      }

      //Methode executer à chaque entrée de caractère
      search(searchbarContent) {
        // On reset le tableau d'item avec tous les items
        this.initializeItems();
      
        // set q to the value of the searchbar
        var q = searchbarContent.srcElement.value;
      
      
        // Si user ne tape rien, on ne filtre pas le tableau on le renvoi intact
        if (!q) {
          return;
        }
      
        this.TabThemesFireBaseSnap = this.TabThemesFireBaseSnap.filter((v) => {
          if(v.titre && q) {
            if (v.titre.toLowerCase().indexOf(q.toLowerCase()) > -1) {
              return true;
            }
            return false;
          }
        });
      
        console.log(q, this.TabThemesFireBaseSnap.length);
      
      }
  //------------------------------Fin Recherche-----------------------------

  //------------------------------- FIREBASE ----------------------------------
  
      //Recuperer un snapshot de la list FIREBASE pour get the key
      getListThemesFirebase() {
        // Use snapshotChanges().map() to store the key
        this.themeFbService.getThemesList().snapshotChanges().map(changes => {
          return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
        }).subscribe(result => {
          //ToDO: Quand la liste a été récupérée
          this.TabThemesFireBaseSnap = result;
          this.loadedThemesList = result;
        
        }); 
      }

      //Methode ajout All programmes sur FIREBASE
      ajoutAllThemesFirebase(){
        this.getThemes();//On get les thèmes du mock et on set dans tab cache this.TabThemesUploadFireBase
        for (let item of this.TabThemesMock) { 
          this.themeFbService.addTheme(item).then(ref => {
          console.log('Le theme '+item.datepost+' a été ajouté à FIREBASE et sa reference est: '+ref);
          })
        }
      }

  //--------------------------------------FIN FIREBASE--------------------------------

  //------------------------------------Autres Methodes--------------------------------

    //Cette methode envoi la clé du thème choisi au component Theme
    sendTheme(th: Theme){
      //TODO: Send the theme in routerlink and go to page component Theme
      this.router.navigate(['/theme/', th.key]);
    }

    //Methodes pour recuperer les thèmes en utilisant le service (Thèmes du mock)
    getThemes(): void {
      this.themeService.getThemes()
          .subscribe(themes => this.TabThemesMock = themes);
    }



  

  //
  // //Recuperer la clé from the template
  // getKeyFromTemplate(key: string){
  //   console.log();
  // }
}










//Méthode qui recupère la liste des programmes from firebase
  // getListThemesFirebase() {
  //   // Use snapshotChanges().map() to store the key
  //   this.themeFbService.getThemesList().snapshotChanges().map(changes => {
  //     return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
  //   }).subscribe(result => {
  //     //ToDO: Quand la liste a été récupérée
  //     this.TabThemesFireBaseSnap = result;
  //     console.log("Voici le tableau de thèmes : "+this.TabThemesFireBaseSnap);
  //   }); 
  // }

  //Methode executer quand on clique sur un theme (bouton lire plus)
  // onSelect(theme: Theme): void {
  //   this.selectedTheme = theme;
  //   //console.log(this.selectedTheme);
  // }


  //Methode recherche autocompletion
  // search($event) {
  //   let q = $event.target.value
  //   this.startAt.next(q);
  //   console.log(this.startAt.next(q));
  //   this.endAt.next(q+"\uf8ff");
  //   console.log(this.endAt.next(q+"\uf8ff"));
  // }