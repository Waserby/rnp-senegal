import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-presentation-partial',
  templateUrl: './presentation-partial.component.html',
  styleUrls: ['./presentation-partial.component.css',
              '../carousel.css']
})
export class PresentationPartialComponent implements OnInit {

  scrollToElement($element): void {
    console.log($element);
    $element.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
  }

  constructor() { }

  ngOnInit() {
  }

}
