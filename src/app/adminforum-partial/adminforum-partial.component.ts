import { Component, OnInit } from '@angular/core';
import { ThemeFBService } from '../theme-fb.service';
import { Theme } from '../../models/theme-model';
import { Router } from '../../../node_modules/@angular/router';

@Component({
  selector: 'app-adminforum-partial',
  templateUrl: './adminforum-partial.component.html',
  styleUrls: ['./adminforum-partial.component.css']
})
export class AdminforumPartialComponent implements OnInit {
  TabThemesFireBaseSnap : any;// Tableau de thèmes from Firebase

  constructor(private router : Router , private themeFbService :ThemeFBService) { }

  ngOnInit() {
    this.getListThemesFirebase(); 
  }

  //------------------------------- FIREBASE ----------------------------------
  
      //Recuperer un snapshot de la list FIREBASE pour get the key
      getListThemesFirebase() {
        // Use snapshotChanges().map() to store the key
        this.themeFbService.getThemesList().snapshotChanges().map(changes => {
          return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
        }).subscribe(result => {
          //ToDO: Quand la liste a été récupérée
          this.TabThemesFireBaseSnap = result;
        
        }); 
      }

  //--------------------------------------FIN FIREBASE--------------------------------

  //Cette methode envoi la clé du thème choisi au component Theme
  sendTheme(th: Theme){
    //TODO: Send the theme in routerlink and go to page component Theme
    this.router.navigate(['/modiftheme/', th.key]);
  }
}
