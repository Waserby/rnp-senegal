import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '../../../node_modules/@angular/forms';
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Component({
  selector: 'app-ontact-partial',
  templateUrl: './ontact-partial.component.html',
  styleUrls: ['./ontact-partial.component.css']
})
export class OntactPartialComponent implements OnInit {
  @Input('backgroundGray') public backgroundGray;
  @ViewChild('ModalMailSuccess') mailModal;
  contactForm: FormGroup;
  FCF_mailUrl = `https://us-central1-rnpweb-45e52.cloudfunctions.net/sendmail`; // L'url de ma fonction d'envoi mail executée sur Firebase Cloud Functions

  constructor(private fb: FormBuilder, private http: HttpClient) { }

  ngOnInit() {
    //Pour le formulaire
    this.contactForm = this.fb.group({
      name: ['', [Validators.required]],
      email: ['', [Validators.email]],
      subject: ['', [Validators.required]],
      message: ['', Validators.required]
    })
    
  }

  //Methode on Submit du formulaire
  onSubmit() {
    //Recupération des champs du formulaire
      // Make sure to create a deep copy of the form-model
      const result: any = Object.assign({}, this.contactForm.value);
      //result.personalData = Object.assign({}, result.personalData);

    // Do useful stuff with the gathered data
  
      // Using Angular HttpClient to send POST request
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'Authorization': 'secret-key'
        })
      };
      console.log(result); //Je regarde ce que j'envoi
      
      this.http.post(this.FCF_mailUrl, result, httpOptions)
      .subscribe(
        res => {
          console.log(res);
          //this.display="block";//On affiche le Modal Mail Success
        },
        err => {
          console.log("Erreur dans l'envoi de la requête POST");
        }
      )
      document.getElementById("openModalButton").click();//Afficher le modal
      this.contactForm.reset();
  }
  //End on submit
  
  //METHODES POUR LE MODAL
  // closeModal() {
     
  // }

}
