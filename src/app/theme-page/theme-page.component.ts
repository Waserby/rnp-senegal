import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '../../../node_modules/@angular/router';
import { Theme } from '../../models/theme-model';
import { Commentaire } from '../../models/commentaire-models';

//Services
import { ThemeService } from '../theme.service';
import { ThemeFBService } from 'src/app/theme-fb.service';
import { NgForm } from '../../../node_modules/@angular/forms';
import { Observable } from '../../../node_modules/rxjs';


@Component({
  selector: 'app-theme-page',
  templateUrl: './theme-page.component.html',
  styleUrls: ['./theme-page.component.css']
})
export class ThemePageComponent implements OnInit {
  //Pour Theme
  theme: any;//C'est le theme courant actuellement afficher (Observable)
  themeObjet : Theme; // C'est le theme courant a afficher (l'Objet))
  themeKey : string; // C'est la clé de l'item courant
  TabThemesFireBaseSnap : any; //List Observables de themes pour side widget

  //Pour Commentaires
  commentairePrincipal : any; 
  commentaireSecondaire : any; 
  collapseReponse : Commentaire;
  pseudoDone : boolean;



  constructor(private route: ActivatedRoute, private themeService: ThemeService, public router : Router, private themeFbService :ThemeFBService) { }

  ngOnInit() {
    //Initialisation
    this.pseudoDone = false;
    this.commentairePrincipal = {pseudo: '', message:''};
    this.commentaireSecondaire = {message:''};

    //Appel des fonctions crées
    this.getTheme();//Recuperation du thème cliqué sur la page forum
    this.getListThemesFirebase(); //recupère les thèmes

  }

  //Recuperer un snapshot de la list FIREBASE pour get the key
  getListThemesFirebase() {
    // Use snapshotChanges().map() to store the key
    this.themeFbService.getThemesList().snapshotChanges().map(changes => {
      return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
    }).subscribe(result => {
      //ToDO: Quand la liste a été récupérée
      this.TabThemesFireBaseSnap = result;
    
    }); 
  }

  //Recupère la clé from route et set theme
    getTheme(): void {
      this.route.params.subscribe(params => {
        let key = params['id']; // Clé from params de route
        this.themeKey = key; // On garde la clé de l'objet
        console.log('La clé    : '+this.themeKey);
        this.theme = this.themeFbService.getThemeById(key).valueChanges();

        //Recuperer l'objet (une capture de sa valeur a cet instant) from l'observable
        this.theme.subscribe(object => {
          this.themeObjet = object;
        }); 
      }); 
    }

  //Methode qui envoi la clé du thème choisi au component THEME avec routerLink
    sendThemeKey(th: Theme){
      //TODO: Send the theme in routerlink and go to page component Theme
        this.router.navigate(['/theme/', th.key]);
    }

  //Methode pour deployer les champs ou on entre la réponse
    collapseRepondre(com : Commentaire){
      this.collapseReponse = com;
    }
    
  
  //-----------------------GESTION DES COMMENTAIRES ----------------------------------

  onSubmit(commentaireForm: NgForm) {
    // Quand c'est a true, je grise le champ pseudo car le user a deja cliqué
      this.pseudoDone = true; 
    //Now je crée la variable commentaire à ajouter au theme courant
      let varcom : Commentaire = {
        id: (this.themeObjet.tabCommentaires.length)+1,//Un new commentaire avec id = lengthTab+1
        pseudo: this.commentairePrincipal.pseudo, //avec pseudo entrer au clavier
        contenu: this.commentairePrincipal.message,//avec message entrer au clavier
        date: new Date().getTime(), //avec date actuelle
        tabReponses: [] //Tableau de reponse vide car nouveau commentaire
      };
    // On push dans le tabCommentaire du thème courant
      this.themeObjet.tabCommentaires.push(varcom);
      console.log("Clé :"+this.themeKey+"  valeur :"+this.themeObjet);
    //On UPDATE le theme courant dans FIREBASE
      this.themeFbService.updateTheme(this.themeKey,this.themeObjet)
       .then(() => { 
          //On reset le champ Message
          this.commentairePrincipal.message = "";
          console.log("Theme ajouté"); 
      });
      console.log(commentaireForm.value);
  }

  //Methode executée quand on valide le commentaire secondaire
  onSubmitSecondaire(commentaireForm: NgForm, com : Commentaire) {
    // Quand c'est a true, je grise le champ pseudo car le user a deja cliqué
      this.pseudoDone = true; 
    //On chercher l'index du commentaire auquelle le user veux repondre
      var indexCom = com.id-1;
    //On initialise le commentaire qui sera rempli et inseré dans le theme pour update
      let commentaireUpdate: Commentaire;

      if(com.tabReponses) {
        //TODO: Si le commentaire avait deja des reponses alors je crée une nouvelle Reponse et j'add au commentaire
        //Now je crée la variable commentaire à ajouter au theme courant
          let varComSecondaire : Commentaire = {
            id: +(com.tabReponses.length)+1,//Un new commentaire avec id = lengthTab+1
            pseudo: this.commentairePrincipal.pseudo, //avec pseudo entrer au clavier
            contenu: this.commentaireSecondaire.message,//avec message entrer au clavier
            date: new Date().getTime(), //avec date actuelle
            tabReponses: [] //Tableau de reponse vide car nouveau commentaire
          };
        //On push dans le tabReponse de com le commentaire secondaire créé
          com.tabReponses.push(varComSecondaire);

        //On met le commentaire fabriqué dans la variable destinée a ca qu'on va ensuite push dans le theme
          commentaireUpdate = com;
      } else {
        //TODO: Si le tableau n'avait pas de tabReponse, je recrée le commentaire sur la base de com mais cette fois avec un tabReponse et j'ajoute la reponse
        let UpdateCommentairePrincipale : Commentaire = {
          id: com.id,//Un new commentaire avec id = lengthTab+1
          pseudo: com.pseudo, //avec pseudo entrer au clavier
          contenu: com.contenu,//avec message entrer au clavier
          date: com.date, //avec date actuelle
          tabReponses: [
            {
              id: 1,
              pseudo: this.commentairePrincipal.pseudo,
              contenu: this.commentaireSecondaire.message,
              date: new Date().getTime(),
              tabReponses: []
            }
          ]
        };
    
        //On met le commentaire fabriqué dans la variable destinée a ca qu'on va ensuite push dans le theme
          commentaireUpdate = UpdateCommentairePrincipale;
      }

    //On remove l'item qu'on creera dans la suite le nouveau com remplacant)
      this.themeObjet.tabCommentaires.splice(indexCom, indexCom+1);
    // On push commenataireUpdate à la place du commentaire du thème courant
      this.themeObjet.tabCommentaires.splice(indexCom, 0, commentaireUpdate);
    //On UPDATE le theme courant dans FIREBASE
      this.themeFbService.updateTheme(this.themeKey,this.themeObjet)
       .then(() => { 
          //On reset le champ Message
          this.commentaireSecondaire.message = "";
          console.log("Commentaire secondaire ajouté"); 
      });
      console.log(commentaireForm.value);
  }
  
}


























// //Methodes pour recuperer les 2 derniers thèmes en utilisant le service
    // get2LastThemes(): void {
    //   this.themeService.getThemes()
    //       .subscribe(themes => this.tabThemes = themes.slice(-2)); //extrait les 2 derniers éléments du tableau slice(1,4) extrait le 2eme 3eme et 4 eme element du tableau 
    // }



// resetForm(commentaireForm?: NgForm) {
  //   if (commentaireForm != null)
  //   commentaireForm.reset();
  //   this.themeFbService.selectedTheme.tabCommentaires = [
  //     {
  //     $key: null,
  //     id: 1,
  //     pseudo: "",
  //     contenu: '',
  //     date: new Date(),
  //     tabReponses: []
  //     }
  //   ]
  // }
  //Methode pour recuperer le thème en utilisant le service qui recoit id et renvoi theme correspondant
    // getTheme(): void {
    //   const key = this.route.snapshot.paramMap.get('key').toString();//On récupère la clé du thème cliqué dans l'url
    //   console.log(this.themeFbService.getThemeByKey(key);
    // }
    // getTheme(): void {
    //   const id = +this.route.snapshot.paramMap.get('id');//On récupère l'id du thème cliqué dans l'url
    //   this.themeService.getTheme(id)
    //     .subscribe(theme => {this.theme = theme;});
    // }



    
    // getThemeThenGoToPath(th : Theme): void {
    //   //TODO: Get theme, set to this.theme and route to page with id i have
    //   this.themeService.getTheme(th.id)
    //     .subscribe(theme => 
    //       {
    //       this.theme = theme;
    //       this.router.navigate(['/theme/'+th.id]); //On navigue vers le path du theme avec l'id recupéré
    //       }
    //     );
    // }
