//Les modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NguCarouselModule } from '../../node_modules/@ngu/carousel';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormsModule } from '../../node_modules/@angular/forms';
import { AppRoutingModule } from './/app-routing.module';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { environment } from '../environments/environment';

//Component
import { AppComponent } from './app.component';
import { PartnersCarousselPartialComponent } from './partners-caroussel-partial/partners-caroussel-partial.component';
import { OntactPartialComponent } from './ontact-partial/ontact-partial.component'; //Contact formulaire
import { VihPageComponent } from './vih-page/vih-page.component';
import { PresentationPartialComponent } from './presentation-partial/presentation-partial.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { CarouselPartialComponent } from './carousel-partial/carousel-partial.component';
import { ForumPageComponent } from './forum-page/forum-page.component';
import { ThemePageComponent } from './theme-page/theme-page.component';

//Angular Material Imports
import { 
  MatIconModule,
  MatCardModule,
  MatMenuModule,
  MatButtonModule,
  MatChipsModule,
  MatListModule,
  MatInputModule,
  MatRippleModule,
  MatSlideToggleModule,
  MatSnackBarModule
 } from '@angular/material';
import { BiblioPageComponent } from './biblio-page/biblio-page.component';
import { AdminPageComponent } from './admin-page/admin-page.component';
import { HttpClientModule } from '../../node_modules/@angular/common/http';
import { ProgrammesPageComponent } from './programmes-page/programmes-page.component';
import { AdminprogrammePartialComponent } from './adminprogramme-partial/adminprogramme-partial.component';
import { AdminmediathequePartialComponent } from './adminmediatheque-partial/adminmediatheque-partial.component';
import { SoutienPageComponent } from './soutien-page/soutien-page.component';
import { AdminforumPartialComponent } from './adminforum-partial/adminforum-partial.component';
import { ModifthemePageComponent } from './modiftheme-page/modiftheme-page.component';
import { AddthemePageComponent } from './addtheme-page/addtheme-page.component';

@NgModule({
  declarations: [
    AppComponent,
    PartnersCarousselPartialComponent,
    OntactPartialComponent,
    VihPageComponent,
    PresentationPartialComponent,
    LandingPageComponent,
    CarouselPartialComponent,
    ForumPageComponent,
    ThemePageComponent,
    BiblioPageComponent,
    AdminPageComponent,
    ProgrammesPageComponent,
    AdminprogrammePartialComponent,
    AdminmediathequePartialComponent,
    SoutienPageComponent,
    AdminforumPartialComponent,
    ModifthemePageComponent,
    AddthemePageComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,//Pour les formulaires
    NguCarouselModule,
    MatIconModule,
    MatCardModule,
    MatMenuModule,
    MatButtonModule,
    MatChipsModule,
    MatListModule,
    MatInputModule,
    MatRippleModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    AppRoutingModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireStorageModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
