import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '../../../node_modules/@angular/router';
import { Subscription } from '../../../node_modules/rxjs';

@Component({
  selector: 'app-vih-page',
  templateUrl: './vih-page.component.html',
  styleUrls: ['./vih-page.component.css',
              './scrolling-nav.css']
})
export class VihPageComponent implements OnInit {

  private scrollExecuted: boolean = false;

  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    if ( !this.scrollExecuted ) {
      // On recoit le fragment from la route active
        this.activatedRoute.fragment
          .subscribe( fragment => {
            //Le set time out fait marcher touut hahaha il fait que après avoir cliquer un lien d'une route (landing ici), quand on clique un
            setTimeout(() => {
              let element = document.getElementById( fragment ); //fragment c'est histo/presi/orga/lead/exp/contact/partners
              if (element) 
              { 
                element.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
                this.scrollExecuted = true; 
              }    
            });
          });
    }
  }

  //Scroll To Element
  scrollToElement($element): void {
    //console.log($element);
    $element.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
  }

}
