import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifthemePageComponent } from './modiftheme-page.component';

describe('ModifthemePageComponent', () => {
  let component: ModifthemePageComponent;
  let fixture: ComponentFixture<ModifthemePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifthemePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifthemePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
