import { Component, OnInit } from '@angular/core';
import { NgForm } from '../../../node_modules/@angular/forms';
import { ThemeFBService } from '../theme-fb.service';
import { Theme } from '../../models/theme-model';

@Component({
  selector: 'app-addtheme-page',
  templateUrl: './addtheme-page.component.html',
  styleUrls: ['./addtheme-page.component.css']
})
export class AddthemePageComponent implements OnInit {
  themeBuild : Theme;

  constructor(private themeFbService: ThemeFBService) { }

  ngOnInit() {
    //this.resetForm();
    console.log(this.themeBuild);
  }

  onSubmit(themeForm: NgForm) {
      this.themeFbService.addTheme(themeForm.value);
  }

  // resetForm(themeForm?: NgForm) {
  //   if (themeForm != null)
  //   themeForm.reset();
  //   this.themeBuild = {
  //     $key: null,
  //     name: '',
  //     position: '',
  //     office: '',
  //     salary: 0,
  //   }
  // }

}
