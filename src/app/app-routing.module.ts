import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

//Components
import { VihPageComponent } from './vih-page/vih-page.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { ForumPageComponent } from './forum-page/forum-page.component';
import { ThemePageComponent } from './theme-page/theme-page.component';
import { BiblioPageComponent } from './biblio-page/biblio-page.component';
import { AdminPageComponent } from './admin-page/admin-page.component';
import { ProgrammesPageComponent } from './programmes-page/programmes-page.component';
import { AdminmediathequePartialComponent } from './adminmediatheque-partial/adminmediatheque-partial.component';
import { AdminprogrammePartialComponent } from './adminprogramme-partial/adminprogramme-partial.component';
import { SoutienPageComponent } from './soutien-page/soutien-page.component';
import { AdminforumPartialComponent } from './adminforum-partial/adminforum-partial.component';
import { ModifthemePageComponent } from './modiftheme-page/modiftheme-page.component';
import { AddthemePageComponent } from './addtheme-page/addtheme-page.component';


const routes: Routes = [
  { path: '', component: LandingPageComponent },
  { path: 'landing', component: LandingPageComponent },
  { path: 'vih', component: VihPageComponent },
  { path: 'forum', component: ForumPageComponent },
  { path: 'theme/:id', component: ThemePageComponent },
  { path: 'addtheme', component: AddthemePageComponent },
  { path: 'modiftheme/:id', component: ModifthemePageComponent },
  { path: 'biblio', component: BiblioPageComponent },
  { path: 'programmes', component: ProgrammesPageComponent },
  { path: 'soutien', component: SoutienPageComponent },
  { path: 'rnpadmin', component: AdminPageComponent },
  { path: 'adminmedia', component: AdminmediathequePartialComponent },
  { path: 'adminprogrammes', component: AdminprogrammePartialComponent },
  { path: 'adminforum', component: AdminforumPartialComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
