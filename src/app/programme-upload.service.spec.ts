import { TestBed, inject } from '@angular/core/testing';

import { ProgrammeUploadService } from './programme-upload.service';

describe('ProgrammeUploadService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProgrammeUploadService]
    });
  });

  it('should be created', inject([ProgrammeUploadService], (service: ProgrammeUploadService) => {
    expect(service).toBeTruthy();
  }));
});
