import { Component, OnInit, Input } from '@angular/core';
import { NguCarousel } from '@ngu/carousel'; // Import le module pour la carousel

@Component({
  selector: 'app-partners-caroussel-partial',
  templateUrl: './partners-caroussel-partial.component.html',
  styleUrls: ['./partners-caroussel-partial.component.css']
})
export class PartnersCarousselPartialComponent implements OnInit {
  @Input('backgroundGray') public backgroundGray;
  public carouselOptions: NguCarousel;
  
  //Le tableau de partenaires
  public partners = [{
    icon: 'dashboard',
    imgurl: `../../assets/img/partenaires/1.jpg`,
  }, {
    icon: 'perm_data_setting',
    imgurl: `../../assets/img/partenaires/5.jpg`,
  }, {
    icon: 'perm_data_setting',
    imgurl: `../../assets/img/partenaires/usaid_x.jpg`,
  }, {
    icon: 'storage',
    imgurl: `../../assets/img/partenaires/7.jpg`,
  }, {
    icon: 'stay_primary_portrait',
    imgurl: `../../assets/img/partenaires/3.jpg`,
  }, {
    icon: 'person',
    imgurl: `../../assets/img/partenaires/2.jpg`,
  }, {
    icon: 'timeline',
    imgurl: `../../assets/img/partenaires/6.jpg`,
  }, {
    icon: 'timeline',
    imgurl: `../../assets/img/partenaires/4.jpg`,
  }, {
    icon: 'timeline',
    imgurl: `../../assets/img/partenaires/8.jpg`,
  }]

  constructor() { }

  ngOnInit() {
    this.carouselOptions = {
      grid: { xs: 1, sm: 2, md: 3, lg: 3, all: 0 },
      slide: 2,
      speed: 400,
      interval: 4000,
      point: {
        visible: true
      },
      load: 2,
      touch: true,
      loop: true
    }
  }

}
