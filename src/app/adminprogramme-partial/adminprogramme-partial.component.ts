import { Component, OnInit } from '@angular/core';
import { ProgrammeUploadService } from '../programme-upload.service';
import { FileUpload } from '../../models/fileupload';

@Component({
  selector: 'app-adminprogramme-partial',
  templateUrl: './adminprogramme-partial.component.html',
  styleUrls: ['./adminprogramme-partial.component.css']
})
export class AdminprogrammePartialComponent implements OnInit {
  selectedFiles: FileList;
  currentFileUpload: FileUpload;
  progress: { percentage: number } = { percentage: 0 };

  //Pour lister les fichiers
  fileUploads: any;
  constructor(private uploadProgramme: ProgrammeUploadService) { }

  ngOnInit() {
    //Pour Lister les fichiers
    // Use snapshotChanges().pipe(map()) to store the key
    this.uploadProgramme.getFileUploads(6).snapshotChanges().map(changes => {
      return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
    }).subscribe(fileUploads => {
      //ToDO: Quand la liste a été récupérée
      this.fileUploads = fileUploads;
    
    }); 
  }
  
  //Cette méthode recupère le fichier selectionné et le garde
  selectFile(event) {
    this.selectedFiles = event.target.files;
  }
  
  //Cette méthode s'execute quand on clique sur le boutton
  upload() {
    const file = this.selectedFiles.item(0);
    this.selectedFiles = undefined;
 
    this.currentFileUpload = new FileUpload(file);
    this.uploadProgramme.pushFileToStorage(this.currentFileUpload, this.progress);
  }

  deleteFileUpload(fileUpload) {
    this.uploadProgramme.deleteFileUpload(fileUpload);
  }

}
