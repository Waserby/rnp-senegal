import { TestBed, inject } from '@angular/core/testing';

import { ThemeFBService } from './theme-fb.service';

describe('ThemeFBService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ThemeFBService]
    });
  });

  it('should be created', inject([ThemeFBService], (service: ThemeFBService) => {
    expect(service).toBeTruthy();
  }));
});
