import { Component, OnInit } from '@angular/core';
import { AngularFireStorage } from '../../../node_modules/angularfire2/storage';
import { UploadFileService } from '../upload-file.service';
import { FileUpload } from '../../models/fileupload';
import { ProgrammeUploadService } from '../programme-upload.service';

@Component({
  selector: 'app-biblio-page',
  templateUrl: './biblio-page.component.html',
  styleUrls: ['./biblio-page.component.css',
              '../carousel.css']
})
export class BiblioPageComponent implements OnInit {
    imgUrl : string;
    fileUploads: any; //Pour lister les fichiers
  //Pour la recherche, on crée un tableau qui contiendra toujours TOUS lesThemes de Firebase (il ne chagne jamais)
    loadedFilesList:Array<any>;

  constructor(private uploadService: UploadFileService ) { }

  ngOnInit() {
    //Pour Lister les fichiers
    // Use snapshotChanges().pipe(map()) to store the key
    this.uploadService.getFileUploads(100).snapshotChanges().map(changes => {
      return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
    }).subscribe(result => {
      //ToDO: Quand la liste a été récupérée
      this.fileUploads = result;//On met tous les fichier dans le tableau cache variable selon la recherche
      this.loadedFilesList = result; //on met les fichiers ici et ils seront toujours fixes

    }); 

  }

  //Methode pour gestion des rubriques
  chooseRubrique(rub : string){
    this.search("any", rub); // On donne la chaine (pdf ou png ou mp4 ou ppt etc) pour la recherche et l'affichage
    //Le any c'est juste n'importe quoi. on aurai pu mettre n'importe quoi 
  }

  //Fonction qui s'execute pour chaque fichier et y associe une url d'img en fonction de l'extension du fichier
  imgtype(file : FileUpload){
    switch(file.name.split(".").pop()) { 
      case "pdf": { 
        this.imgUrl="../../assets/img/mediatheque/pdf.png";
        break; 
      } 
      case "ppt": { 
        this.imgUrl="../../assets/img/mediatheque/ppt.png";
        break; 
      } 
      case "jpg": {
        this.imgUrl="../../assets/img/mediatheque/jpg.png";
        break;    
      } 
      case "png": { 
        this.imgUrl="../../assets/img/mediatheque/png.png";
        break; 
      }  
      default: { 
         console.log("Invalid choice"); 
         break;              
      } 
   }
  }

  //------------------------------Pour la recherche----------------------------------
  initializeItems(): void {
    this.fileUploads = this.loadedFilesList;
  }

  //Methode executer à chaque entrée de caractère
  search(searchbarContent, chaine ?: string) {
    // On reset le tableau d'item avec tous les items
    this.initializeItems();
  
    // On met ce que la user a tapé dans q selon que on a recu la valeur chaine directement ou searchbarContent duquel on tire la chaine avec searchbarContent.srcElement.value
    var q;
    if(searchbarContent == "any")
      {
        q = chaine;
      }else{
        q = searchbarContent.srcElement.value;
      }
    // Si user ne tape rien, on ne filtre pas le tableau on le renvoi intact
    if (!q) {
      return;
    }
  
    this.fileUploads = this.fileUploads.filter((v) => {
      if(v.name && q) {
        if (v.name.toLowerCase().indexOf(q.toLowerCase()) > -1) {
          return true;
        }
        return false;
      }
    });
  
    console.log(q, this.fileUploads.length);
  
  }
//------------------------------Fin Recherche-----------------------------

}
