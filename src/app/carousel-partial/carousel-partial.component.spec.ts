import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarouselPartialComponent } from './carousel-partial.component';

describe('CarouselPartialComponent', () => {
  let component: CarouselPartialComponent;
  let fixture: ComponentFixture<CarouselPartialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarouselPartialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarouselPartialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
