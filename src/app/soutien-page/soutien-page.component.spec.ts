import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SoutienPageComponent } from './soutien-page.component';

describe('SoutienPageComponent', () => {
  let component: SoutienPageComponent;
  let fixture: ComponentFixture<SoutienPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SoutienPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SoutienPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
