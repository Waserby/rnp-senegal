import { Component, OnInit } from '@angular/core';
import { FileUpload } from '../../models/fileupload';
import { UploadFileService } from '../upload-file.service';
import { ProgrammeUploadService } from '../programme-upload.service';

@Component({
  selector: 'app-programmes-page',
  templateUrl: './programmes-page.component.html',
  styleUrls: ['./programmes-page.component.css',
              '../carousel.css']
})
export class ProgrammesPageComponent implements OnInit {
  imgUrl : string;
  fileUploads: any; //Pour lister les fichiers
  loadedFilesList:Array<any>; //Pour la recherche, on crée un tableau qui contiendra toujours TOUS les Themes de Firebase (il ne chagne jamais)

  constructor(private uploadProgramme: ProgrammeUploadService) { }

  ngOnInit() {
    this.uploadProgramme.getFileUploads(100).snapshotChanges().map(changes => {
      return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
    }).subscribe(result => {
      //ToDO: Quand la liste a été récupérée
      this.fileUploads = result;//On met tous les fichier dans le tableau cache variable selon la recherche
      this.loadedFilesList = result; //on met les fichiers ici et ils seront toujours fixes

    }); 
  }

  //Fonction qui s'execute pour chaque fichier et y associe une url d'img en fonction de l'extension du fichier
  imgtype(file : FileUpload){
    switch(file.name.split(".").pop()) { 
      case "pdf": { 
        this.imgUrl="../../assets/img/mediatheque/pdf.png";
        break; 
      } 
      case "ppt": { 
        this.imgUrl="../../assets/img/mediatheque/ppt.png";
        break; 
      } 
      case "jpg": {
        this.imgUrl="../../assets/img/mediatheque/jpg.png";
        break;    
      } 
      case "png": { 
        this.imgUrl="../../assets/img/mediatheque/png.png";
        break; 
      }  
      case "xlsx": { 
        this.imgUrl="../../assets/img/mediatheque/xlsx.png";
        break; 
      }  
      case "xls": { 
        this.imgUrl="../../assets/img/mediatheque/xlsx.png";
        break; 
      }  
      default: {
        console.log("ivalid") 
         break;              
      } 
   }
  }

//------------------------------Pour la recherche----------------------------------
  initializeItems(): void {
    this.fileUploads = this.loadedFilesList;
  }

  //Methode executer à chaque entrée de caractère
  search(searchbarContent) {
    // On reset le tableau d'item avec tous les items
    this.initializeItems();
  
    // On met ce que la user a tapé dans q
    var q = searchbarContent.srcElement.value;
  
  
    // Si user ne tape rien, on ne filtre pas le tableau on le renvoi intact
    if (!q) {
      return;
    }
  
    this.fileUploads = this.fileUploads.filter((v) => {
      if(v.name && q) {
        if (v.name.toLowerCase().indexOf(q.toLowerCase()) > -1) {
          return true;
        }
        return false;
      }
    });
  
    console.log(q, this.fileUploads.length);
  
  }
//------------------------------Fin Recherche-----------------------------

}
