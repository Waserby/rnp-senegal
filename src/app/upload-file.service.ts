import { Injectable } from '@angular/core';
import { FileUpload } from '../models/fileupload';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import * as firebase from 'firebase/app';
import 'firebase/storage';

@Injectable({
  providedIn: 'root'
})
export class UploadFileService {
  private basePath = '/mediatheque';

  constructor(private db: AngularFireDatabase) { }

  //Méthode qui upload un fichier sur le STORAGE et l'ajoute aussi à la Liste Firebase
  pushFileToStorage(fileUpload: FileUpload, progress: { percentage: number }) {
    const storageRef = firebase.storage().ref();
    const uploadTask = storageRef.child(`${this.basePath}/${fileUpload.file.name}`).put(fileUpload.file);
 
    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
      (snapshot) => {
        // in progress
        const snap = snapshot as firebase.storage.UploadTaskSnapshot;
        progress.percentage = Math.round((snap.bytesTransferred / snap.totalBytes) * 100);
      },
      (error) => {
        // fail
        console.log(error);
      },
      () => {
        // success : Dans ce cas on recupère l'URL du 
        uploadTask.snapshot.ref.getDownloadURL().then(downloadURL => {
          console.log('URL du fichier uploadé : ', downloadURL);
          fileUpload.url = downloadURL;
          fileUpload.name = fileUpload.file.name;
          this.saveFileData(fileUpload);//si le upload a réussi, on push les datas du fichier sur Real Time Database
        });
      }
    );
  }
 
  //LIST: Methode qui ajoute un fichier à la liste
  private saveFileData(fileUpload: FileUpload) {
    this.db.list(`${this.basePath}/`).push(fileUpload);
  }
 
  //MEthode qui renvoi les fichiers uploadés
  getFileUploads(numberItems): AngularFireList<FileUpload> {
    return this.db.list(this.basePath, ref =>
      ref.limitToLast(numberItems));
  }
 
  //Methode à utiliser pour supprimer un fichier de Storage et de la List
  deleteFileUpload(fileUpload: FileUpload) {
    this.deleteFileDatabase(fileUpload.key)
      .then(() => {
        this.deleteFileStorage(fileUpload.name);
      })
      .catch(error => console.log(error));
  }
 
  //LIST: Methode qui supprime une entrée de la liste à partir de sa clé
  private deleteFileDatabase(key: string) {
    return this.db.list(`${this.basePath}/`).remove(key);
  }
  //STORAGE: Methode qui supprime un fichier du Storage
  private deleteFileStorage(name: string) {
    const storageRef = firebase.storage().ref();
    storageRef.child(`${this.basePath}/${name}`).delete();
  }
}
