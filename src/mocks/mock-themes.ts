import { Theme } from "../models/theme-model";
import { Commentaire } from "../models/commentaire-models";


export const THEMES: Theme[] = [
  { 
    id: 1, 
    auteur: "waserby",
    titre: "Les retro-antiviraux", 
    description: "Selon les résultats anticipés d'une étude du gouvernement américain, la prise de rétro-antiviraux chez les patients infectés réduirait les risques de transmission du Sida.", 
    tabcontenu:[
      "Les différents antirétroviraux que l'on retrouve en pharmacie disposent d'un mode d'action spécifique. Chaque produit antirétroviral dispose de molécules spécifiques qui permettent d'agir sur un rétrovirus. Certaines molécules entrent en compétition avec d'autres nucléosides tels que la thymidine triphosphate.",
      "En rendant inactif le composant, la transcriptase inverse (enzyme rétrovirus) n'est plus en mesure de procéder à l'élongation du génome viral ADN. D'autres molécules procèdent par inhibition de la transcriptase, mais n'emploient pas la même méthode. En effet, certaines molécules se fixent sur des résidus amino-acides et altèrent leur composition.",
      "En dehors de la transcriptase, les molécules antirétrovirales sont des inhibiteurs de la protéase virale. Ces molécules disposent d'une composition chimique proche du peptide. Elles perturbent principalement l'activité protéolytique du virus.",
      "Des molécules récentes utilisent une action virostatique particulière. Il s'agit des inhibiteurs de la fusion. Elles stoppent l'activité fusiogène des rétrovirus en se fixant sur la glycoprotéine 41 (gp41). Le fonctionnement des différents antiviraux a permis de classifier les molécules ayant des propriétés antirétrovirales."    
    ], 
    datepost: new Date("August 29, 2018 13:00:00").getTime(), 
    urlimg: "../assets/img/forum/antiviraux_x.jpg",
    tabCommentaires: [
      {
      id: 1,
      pseudo: "Sasuke",
      contenu: 'I ponder of something great',
      date: new Date("August 4, 2016 10:13:00").getTime(),
      tabReponses: []
      }
    ]
  },
  { 
    id: 2, 
    auteur: "AIDS-Hilfe Schweiz",
    titre: "La recherche donne des signaux encourageants", 
    description: "Des études récentes et les derniers résultats de la recherche mondiale y ont été présentés durant quatre jours. Retour sur quelques nouveautés inté- ressant les personnes vivant avec le VIH.",
    tabcontenu:[
      "Il reste néanmoins de gros défis à relever: comment faire accéder au traitement les quelque 17 millions de personnes restantes? Le traitement des personnes séropositives reste in- suffisant dans certaines régions du monde, comme l’ont rappelé des activistes venus du Venezuela à l’occasion de la conférence. S’adressant aux décideurs issus de la recherche, de l’industrie et du monde politique, ils ont lancé un appel à l’aide et réclamé un renforce- ment de l’engagement international contre la crise d’approvisionnement dans leur pays.",
      "Et comment assurer la continuité du traitement pour les personnes vivant avec le VIH? Un grand nombre de personnes sont diagnostiquées séropositives, mais ne sont ensuite pas suffisamment orientées en vue d’un traitement ou alors elles l’interrompent. Les rai- sons de cette interruption sont par exemple le travail, l’éloignement des médecins ou des pharmacies, le sentiment d’être en bonne santé ou encore la stigmatisation qui entoure toujours le VIH. En outre, les lois qui, dans certains pays, criminalisent la consommation de drogues ou le sexe entre hommes entravent l’accès au traitement pour les personnes concernées.",
      "Le traitement centré sur le patient offre des solutions. Les clubs destinés à stimuler l’adhésion au traitement en sont un exemple. Entre 15 et 30 personnes vivant avec le VIH s’y rencontrent tous les deux mois en dehors des structures médicales et sous la direction de travailleurs sociaux. Les médicaments sont distribués à cette occasion, les personnes se motivent mutuellement à poursuivre le traitement et, en cas de troubles ou d’effets secondaires, elles sont redirigées vers la clinique la plus proche. Comme ces clubs sont plus proches des domiciles, les personnes s’y rendent plus régulièrement que dans les structures médicales. Ces clubs fonctionnent très bien en particulier pour les femmes. On continue à chercher des solutions afin d’améliorer l’observance thérapeu- tique chez les jeunes et chez les hommes. ",
      "De meilleures options de traitement antirétroviral, de bons systèmes de santé et une meilleure couverture, la qualité et l’efficacité du traitement, voilà les priorités pour le traitement futur des personnes vivant avec le VIH. Il faut en outre mieux prendre en compte les attentes et le ressenti des personnes dans le traitement. Cela signifie mieux chercher à savoir comment elles prennent leur traitement, quelles sont les stratégies qu’elles appliquent en matière de santé, quels sont les obstacles sur lesquels elles butent au quotidien. Enfin, l’opinion des utilisateurs doit être intégrée déjà au stade du plan de recherche et du développement de produits.",
    ], 
    datepost: new Date("February 4, 2016 10:13:00").getTime(), 
    urlimg: "../assets/img/forum/tube_x.jpg",
    tabCommentaires: [
        {
        id: 1,
        pseudo: "Sasuke",
        contenu: 'I got the Mangekyou SHARINGAN !!!',
        date: new Date("February 4, 2016 10:13:00").getTime(),
        tabReponses: [
          {
            id: 1,
            pseudo: "Pain",
            contenu: 'Rinnegan is powerfull than Sharingan',
            date: new Date("February 4, 2016 10:13:00").getTime(),
            tabReponses: []
          }
        ]
        },
        {
        id: 2,
        pseudo: "Jiraiya",
        contenu: 'I taught Rasengan to this stupid Naruto',
        date: new Date("February 4, 2016 10:13:00").getTime(),
        tabReponses: [
          {
            id: 1,
            pseudo: "Naruto",
            contenu: 'Hahaha and i turn it to Odamaaaa Rasengan !!',
            date: new Date("February 4, 2016 10:20:00").getTime(),
            tabReponses: []
          },
          {
            id: 2,
            pseudo: "Bee Sama",
            contenu: 'Bakayaruuu Konoyaruuu !!! Yeahh !! ',
            date: new Date("February 4, 2016 10:20:00").getTime(),
            tabReponses: []
          }
        ]
        }
    ]
  }
];