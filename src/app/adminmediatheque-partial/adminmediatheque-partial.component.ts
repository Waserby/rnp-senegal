import { Component, OnInit } from '@angular/core';
import { AngularFireStorage, AngularFireStorageReference, AngularFireUploadTask } from '../../../node_modules/angularfire2/storage';
import { Observable } from '../../../node_modules/rxjs';
import { UploadFileService } from '../upload-file.service';
import { FileUpload } from '../../models/fileupload';
 
@Component({
  selector: 'app-adminmediatheque-partial',
  templateUrl: './adminmediatheque-partial.component.html',
  styleUrls: ['./adminmediatheque-partial.component.css']
})
export class AdminmediathequePartialComponent implements OnInit {
  selectedFiles: FileList;
  currentFileUpload: FileUpload;
  progress: { percentage: number } = { percentage: 0 };

  //Pour lister les fichiers
  fileUploads: any;

  constructor(private uploadService: UploadFileService){ }

  ngOnInit() {
    //Pour Lister les fichiers
    // Use snapshotChanges().pipe(map()) to store the key
    this.uploadService.getFileUploads(6).snapshotChanges().map(changes => {
      return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
    }).subscribe(fileUploads => {
      //ToDO: Quand la liste a été récupérée
      this.fileUploads = fileUploads;
    
    }); 
  }
  
  //Cette méthode recupère le fichier selectionné et le garde
  selectFile(event) {
    this.selectedFiles = event.target.files;
  }
  
  //Cette méthode s'execute quand on clique sur le boutton
  upload() {
    const file = this.selectedFiles.item(0);
    this.selectedFiles = undefined;
 
    this.currentFileUpload = new FileUpload(file);
    this.uploadService.pushFileToStorage(this.currentFileUpload, this.progress);
  }

  deleteFileUpload(fileUpload) {
    this.uploadService.deleteFileUpload(fileUpload);
  }

}
