import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnersCarousselPartialComponent } from './partners-caroussel-partial.component';

describe('PartnersCarousselPartialComponent', () => {
  let component: PartnersCarousselPartialComponent;
  let fixture: ComponentFixture<PartnersCarousselPartialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnersCarousselPartialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnersCarousselPartialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
