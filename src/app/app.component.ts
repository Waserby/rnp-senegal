import { Component, ViewChild, ElementRef } from '@angular/core';
import { Router } from '../../node_modules/@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'rnp-ng';
  @ViewChild('navbarToggler') navbarToggler:ElementRef;

  constructor(private router : Router) { }

  //Scroller vers un element de la page NOT USE NOW
    scrollToElement($element): void {
      console.log($element);
      $element.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
    }

  //Fonction qui envoi une chaine particuliere à chaque clic de lien. C'est pour scroller vers la partie cliqué dans la page de destination
    goToPage (link :string, section: string){
      //TODO: La section c'est la partie ou on doit positionné le scroll et link c'est la page
      this.collapseNav();
      this.router.navigate( [ '/'+link ], { fragment: section } );
      console.log("gotopage DONE");
      // this.router.navigate(['/'+link, section]);
    }

  // ---------------------Code pour fermer le menu quand on clique une rubrique----------------------
    navBarTogglerIsVisible() {
      // Renvoi True si le menu est ouvert
      return this.navbarToggler.nativeElement.offsetParent !== null;
    }

    // Methode executée quand on clique sur n'importe quel link
    collapseNav() {
      // var element = document.getElementById("partners");
      // this.scrollToElement(element);
      //Si le menu est ouvert, on le ferme
      if (this.navBarTogglerIsVisible()) {
        this.navbarToggler.nativeElement.click();
      }
    }
  //---------------------------------------Fin fermeture menu----------------------------------------
}
