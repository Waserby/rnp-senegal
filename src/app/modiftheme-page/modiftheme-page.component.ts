import { Component, OnInit } from '@angular/core';
import { Theme } from '../../models/theme-model';
import { ActivatedRoute, Router } from '../../../node_modules/@angular/router';
import { ThemeFBService } from '../theme-fb.service';

@Component({
  selector: 'app-modiftheme-page',
  templateUrl: './modiftheme-page.component.html',
  styleUrls: ['./modiftheme-page.component.css']
})
export class ModifthemePageComponent implements OnInit {
  //Pour Theme
    theme: any;//C'est le theme courant actuellement afficher (Observable)
    themeObjet : Theme; // C'est le theme courant a afficher (l'Objet))
    themeKey : string; // C'est la clé de l'item courant
    TabThemesFireBaseSnap : any; //List Observables de themes pour side widget

  constructor(public router : Router, private route: ActivatedRoute, private themeFbService :ThemeFBService) { }

  ngOnInit() {
    //Appel des fonctions crées
    this.getTheme();//Recuperation du thème cliqué sur la page forum
    this.getListThemesFirebase(); //recupère les thèmes
  }

  //Recuperer un snapshot de la list FIREBASE pour get the key
  getListThemesFirebase() {
    // Use snapshotChanges().map() to store the key
    this.themeFbService.getThemesList().snapshotChanges().map(changes => {
      return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
    }).subscribe(result => {
      //ToDO: Quand la liste a été récupérée
      this.TabThemesFireBaseSnap = result;
    
    }); 
  }


  //Recupère la clé from route et set theme
  getTheme(): void {
    this.route.params.subscribe(params => {
      let key = params['id']; // Clé from params de route
      this.themeKey = key; // On garde la clé de l'objet
      console.log('La clé : '+this.themeKey);
      this.theme = this.themeFbService.getThemeById(key).valueChanges();

      //Recuperer l'objet (une capture de sa valeur a cet instant) from l'observable
      this.theme.subscribe(object => {
        this.themeObjet = object;
      }); 
    }); 
  }

  //Methode qui envoi la clé du thème choisi au component THEME avec routerLink
  sendThemeKey(th: Theme){
    //TODO: Send the theme in routerlink and go to page component Theme
      this.router.navigate(['/modiftheme/', th.key]);
  }
}
