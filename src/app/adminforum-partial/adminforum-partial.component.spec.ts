import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminforumPartialComponent } from './adminforum-partial.component';

describe('AdminforumPartialComponent', () => {
  let component: AdminforumPartialComponent;
  let fixture: ComponentFixture<AdminforumPartialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminforumPartialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminforumPartialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
