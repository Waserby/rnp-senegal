import { Injectable } from '@angular/core';
import { AngularFireList, AngularFireDatabase, AngularFireObject} from '../../node_modules/angularfire2/database';
import { Theme } from '../models/theme-model';
import { Commentaire } from '../models/commentaire-models';
import { Observable } from '../../node_modules/rxjs';

@Injectable({
  providedIn: 'root'
})
export class ThemeFBService {
  private themesList = this.db.list<Theme>("themes-list");
  themes$: Observable<any[]>;//Observables pour pouvoir obtenir les clés

  
  constructor(private db :AngularFireDatabase) { 
    this.themes$ = this.db.list<Theme>("themes-list").snapshotChanges().map(changes => {
      return changes.map(c => ({ key: c.payload.key, ...c.payload.val() 
      }));
    });
  }


  //-----------------------------CRUD--FIREBASE-------------------------
  getThemesList() {
    return this.themesList;
  }

  //Recherche dynamiquement un thème dans Firebase et renvoi la liste de thème correspondante
  // getThemesSearch(start, end): AngularFireList<Theme> {
  //   return this.db.list('/themes-list', ref => ref.orderByChild('titre').limitToFirst(5).startAt(start).endAt(end))
  // }
  
  // {
    // query: {
    //   orderByChild: 'Title',
    //   limitToFirst: 10,
    //   startAt: start,
    //   endAt: end
    // }
  // getThemesSearch(start, end): AngularFireList<any> {
  //   return this.db.list('/themes-list', ref => { 
  //     let query = ref.orderByChild("titre").limitToFirst(10).startAt(start).endAt(end);
  //     return query;
  //   });
  // }

  addTheme(item: Theme){
    return this.themesList.push(item);
  }

  updateTheme(key: string, item: Theme) {
    return this.themesList.update(key, item);
  }

  removeTheme(item: Theme) {
    return this.themesList.remove(item.key);
  }
  //-------------END-CRUD-FIREBASE------------------
  
  getThemeById(key: string){
    return this.db.object<Theme>('themes-list/' + key);
  }

  // getThemeByKey(key: string): AngularFireObject<Theme> {
  //   const itemPath =  'themes-list/'+{key};
  //   return this.db.object(itemPath);
  // }
  
}












// this.selectedTheme = {
//   $key:null, 
//   id: 1, 
//   auteur: "waserby",
//   titre: "Les retro-antiviraux", 
//   description: "Selon les résultats anticipés d'une étude du gouvernement américain, la prise de rétro-antiviraux chez les patients infectés réduirait les risques de transmission du Sida.", 
//   tabcontenu:[
//     "Les différents antirétroviraux que l'on retrouve en pharmacie disposent d'un mode d'action spécifique. Chaque produit antirétroviral dispose de molécules spécifiques qui permettent d'agir sur un rétrovirus. Certaines molécules entrent en compétition avec d'autres nucléosides tels que la thymidine triphosphate.",
//     "En rendant inactif le composant, la transcriptase inverse (enzyme rétrovirus) n'est plus en mesure de procéder à l'élongation du génome viral ADN. D'autres molécules procèdent par inhibition de la transcriptase, mais n'emploient pas la même méthode. En effet, certaines molécules se fixent sur des résidus amino-acides et altèrent leur composition.",
//     "En dehors de la transcriptase, les molécules antirétrovirales sont des inhibiteurs de la protéase virale. Ces molécules disposent d'une composition chimique proche du peptide. Elles perturbent principalement l'activité protéolytique du virus.",
//     "Des molécules récentes utilisent une action virostatique particulière. Il s'agit des inhibiteurs de la fusion. Elles stoppent l'activité fusiogène des rétrovirus en se fixant sur la glycoprotéine 41 (gp41). Le fonctionnement des différents antiviraux a permis de classifier les molécules ayant des propriétés antirétrovirales."    
//   ], 
//   datepost: new Date("August 29, 2018 13:00:00"), 
//   urlimg: "../assets/img/forum/antiviraux_x.jpg",
//   tabCommentaires: [
//     {
//     $key:null,
//     id: 1,
//     pseudo: "Sasuke",
//     contenu: 'I ponder of something great',
//     date: new Date("Aout 4, 2016 10:13:00"),
//     tabReponses: []
//     }
//   ]
// };






//---------------------CRUD FIREBASE----------------------------------
// getAllThemes(){
//   this.themeList = this.db.list('themes');
//   return this.themeList;
// }

// insertTheme(theme : Theme)
// {
//   this.themeList.push({
//     id: theme.id,
//     auteur: theme.auteur,
//     titre: theme.titre,
//     description: theme.description,
//     tabcontenu: theme.tabcontenu,
//     datepost: theme.datepost,
//     urlimg: theme.urlimg,
//     tabCommentaires: theme.tabCommentaires
//   });
// }

// // insertCommentaire(commentaire: Commentaire){
// //   this.themeList.push({
// //     id: theme.id,
// //     auteur: theme.auteur,
// //     titre: theme.titre,
// //     description: theme.description,
// //     tabcontenu: theme.tabcontenu,
// //     datepost: theme.datepost,
// //     urlimg: theme.urlimg,
// //     tabCommentaires: theme.tabCommentaires
// //   });
// // }

// updateTheme(theme : Theme){
//   this.themeList.update(theme.$key,
//     {
//       id: theme.id,
//       auteur: theme.auteur,
//       titre: theme.titre,
//       description: theme.description,
//       tabcontenu: theme.tabcontenu,
//       datepost: theme.datepost,
//       urlimg: theme.urlimg,
//       tabCommentaires: theme.tabCommentaires
//     });
// }

// deletetheme($key : string){
//   this.themeList.remove($key);
// }