import { Commentaire } from "./commentaire-models";

export class Theme {
    key?:string;
    id: number;
    auteur: String;
    titre: String;
    description: String;
    tabcontenu:String[];
    datepost: number;
    urlimg: String;
    tabCommentaires: Commentaire[];
}