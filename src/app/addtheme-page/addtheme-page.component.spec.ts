import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddthemePageComponent } from './addtheme-page.component';

describe('AddthemePageComponent', () => {
  let component: AddthemePageComponent;
  let fixture: ComponentFixture<AddthemePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddthemePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddthemePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
