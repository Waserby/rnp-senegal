import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VihPageComponent } from './vih-page.component';

describe('VihPageComponent', () => {
  let component: VihPageComponent;
  let fixture: ComponentFixture<VihPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VihPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VihPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
