import { Component, OnInit, AfterViewChecked } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '../../../node_modules/@angular/router';
import { Subscription } from '../../../node_modules/rxjs';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css',
              '../carousel.css']
})
export class LandingPageComponent implements OnInit,AfterViewChecked {

  private scrollExecuted: boolean = false;

  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit() {

    if ( !this.scrollExecuted ) {
      let routeFragmentSubscription: Subscription;

      // On recoit le fragment from la route active
      routeFragmentSubscription =
        this.activatedRoute.fragment
          .subscribe( fragment => {
            //Le set time out fait marcher touut hahaha il fait que après avoir cliquer un lien d'une route (landing ici), quand on clique un
            setTimeout(() => {
              let element = document.getElementById( fragment ); //fragment c'est histo/presi/orga/lead/exp/contact/partners
              if (element) 
              { 
                element.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
                this.scrollExecuted = true; 
              }    
            });
          });
    }
  }//Ng On Init


  ngAfterViewChecked() {

  }//Fin Ng AfterView Checked

  //Scroll To Element
  scrollToElement($element): void {
    //console.log($element);
    $element.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
  }

  goTo(section :string){
    var target = document.getElementById(section);//Afficher le modal
    //console.log(target);
    this.scrollToElement(target);
  }
}



// GRATINN
// ngAfterViewChecked(){
  //   if ( !this.scrollExecuted ) 
  //   {
  //     let routeSectionSubscription : Subscription;

  //     routeSectionSubscription = this.route.params.subscribe(params => {
  //       let section = params['section']; // Clé from params de route
  //       //console.log("La section : "+section);

  //       if(section !== undefined){ 
  //         var element = document.getElementById(section);
  //         //console.log(element);
  //         this.scrollToElement(element);
  //         this.scrollExecuted = true;

  //         // Free resources
  //         setTimeout(
  //           () => {
  //             console.log( 'routeFragmentSubscription unsubscribe' );
  //             routeSectionSubscription.unsubscribe();
  //         }, 1000 );
  //       }
        
  //     });
  //   } //Fin if pas encore scroll
  // }

