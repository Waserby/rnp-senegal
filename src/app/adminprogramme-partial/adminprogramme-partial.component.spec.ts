import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminprogrammePartialComponent } from './adminprogramme-partial.component';

describe('AdminprogrammePartialComponent', () => {
  let component: AdminprogrammePartialComponent;
  let fixture: ComponentFixture<AdminprogrammePartialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminprogrammePartialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminprogrammePartialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
