import { Injectable } from '@angular/core';
import { Observable, of } from '../../node_modules/rxjs';
import { Theme } from '../models/theme-model';
import { THEMES } from '../mocks/mock-themes';

@Injectable({
  providedIn: 'root'//C'est pour dire que le service est rendu disponible à tout component qui en aura besoin
})
export class ThemeService {

  constructor() { }

  //Methode qui renvoi tous les THEMES du mock
  getThemes(): Observable<Theme[]> {
    return of(THEMES);// On renvoi un observable de thème
  }

  getTheme(id: number): Observable<Theme> {
    // TODO: Renvoyer le theme correspondant à l'id en entrée
    return of(THEMES.find(theme => theme.id === id));
  }

  //Méthode renvoyant les 3 thèmes les plus récents

}
