import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PresentationPartialComponent } from './presentation-partial.component';

describe('PresentationPartialComponent', () => {
  let component: PresentationPartialComponent;
  let fixture: ComponentFixture<PresentationPartialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PresentationPartialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PresentationPartialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
