import { Component, OnInit } from '@angular/core';
import { AngularFireStorage, AngularFireStorageReference, AngularFireUploadTask } from '../../../node_modules/angularfire2/storage';
import { Observable } from '../../../node_modules/rxjs';
import { UploadFileService } from '../upload-file.service';
import { FileUpload } from '../../models/fileupload';
 
@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.css']
})
export class AdminPageComponent implements OnInit {

  constructor(){ }

  ngOnInit() {
    
  }
  
}
