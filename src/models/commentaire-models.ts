export class Commentaire {
    id: number;
    pseudo: String;
    contenu: String;
    date: number;
    tabReponses: Commentaire[];
}